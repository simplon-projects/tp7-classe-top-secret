<?php
session_start();

    require_once './php/manager/MissionManager.php';
    require_once './php/manager/AgentManager.php';
    require_once './php/manager/TargetManager.php';
    require_once './php/manager/ContactManager.php';
    require_once './php/manager/HideoutManager.php';
    require_once './php/manager/StatusManager.php';

    $missionManager = new MissionManager();
    $missions = $missionManager->getAll();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title></title>
    
    <meta name="title" content="">
    <meta name="description" content=""> 
    
    <!--link rel="icon" type="image/png" sizes="32x32" href=""-->

    <!--feuille de style-->
    <link rel="stylesheet" href="./assets/css/style-prod.css">
    <!--fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@400;700&display=swap" rel="stylesheet"> 
</head>

<body>

    
    <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand d-flex font-weight-bold" href="./index.php">The secret Agency</a>  
        <a id="iconLogin" href="./login_page.php">
            <svg width="22px" height="22px" viewBox="0 0 16 16" class="bi bi-lock-fill" fill="white" xmlns="http://www.w3.org/2000/svg">
            <path d="M2.5 9a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-7a2 2 0 0 1-2-2V9z"/>
            <path fill-rule="evenodd" d="M4.5 4a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z"/>
            </svg>
        </a>
    </nav>

    <div id="jumbotron" class="container-fluid text-light">
        <div class="ml-5">
            <h1 class="display-4">Welcome to the Secret Agency</h1>
            <p class="lead my-3">We are the best agency in the whole World. You can consult the details of our missions.</p>
            <a class="btn btn-warning rounded-0 btn-lg" href="#Mission" role="button">See our missions</a>
        </div>
    </div>

    <div id="Mission" class="container my-5">

        <?php
            foreach ($missions as $mission) {
        ?>
        <div class="missionDetails border my-3">
            <h2 class="bg-dark text-light p-3"><?= $mission->getTitle(); ?></h2>
            <div class="p-3">
                <p><span class="font-weight-bold">Codename : </span><?= $mission->getCodename(); ?></p>
                <p><span class="font-weight-bold">Mission type : </span><?= $mission->getType(); ?></p>
                <p><span class="font-weight-bold">Description : </span><?= $mission->getDescription(); ?></p>
                <p><span class="font-weight-bold">Start date : </span><?= date("d F Y", strtotime($mission->getStartdate())); ?></p>
                <p><span class="font-weight-bold">End date : </span><?= date("d F Y", strtotime($mission->getEnddate())); ?></p>
                <p><span class="font-weight-bold">Country : </span><?= $mission->getCountry(); ?></p>
                <p><span class="font-weight-bold">Required skills : </span><?=implode( ", ", $mission->getSkills()); ?></p>
                <a class="p-0 my-2 btn text-warning font-weight-bold" data-toggle="modal" data-target="#<?= str_replace(' ', '', $mission->getCodename()); ?>">
                    <u>Mission status</u>
                </a></br>
                <a class="p-0 my-2 btn text-warning font-weight-bold" data-toggle="collapse" href="#<?= str_replace(' ', '', $mission->getCodename()); ?>Details" role="button" aria-expanded="false" aria-controls="<?= str_replace(' ', '', $mission->getCodename()); ?>Details">
                <u>Mission details</u>
                </a>
            </div>

            <div class="modal fade" id="<?= str_replace(' ', '', $mission->getCodename()); ?>" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content rounded-0">
                    <div class="modal-header">
                        <h5 class="modal-title"><?= $mission->getTitle(); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped table-dark">
                            <thead>
                                <tr>
                                <th scope="col">Codename</th>
                                <th scope="col">Date</th>
                                <th scope="col">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $statusManager = new StatusManager();
                                $statuses = $statusManager->getAll($mission->getCodename());
                                foreach ($statuses as $status) {
                                ?>
                                        <tr>
                                        <th scope="row"><?=$mission->getCodename(); ?></th>
                                        <td><?=date("d F Y", strtotime($status->getDate())); ?></td>
                                        <td><?=$status->getLabel(); ?></td>
                                        </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
            
            <div class="collapse" id="<?= str_replace(' ', '', $mission->getCodename()); ?>Details">
                <div class="p-3 d-flex flex-wrap">
                    <?php
                    $agents = $mission->getAgents();
                    foreach ($agents as $agent_required) {
                        $agentManager = new AgentManager();
                        $agent = $agentManager->getRequiredAgent($agent_required);
                    ?>
                        <div class="missionPerson text-dark m-1 p-2">
                            <div class="text-center mb-2">
                                <svg class="m-1" width="80px" height="80px" viewBox="0 0 16 16" class="bi bi-person-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                </svg>
                                <p class="personType text-uppercase font-weight-bold">Agent</p>
                            </div>

                            <p><?= $agent->getFirstname(); ?> <?= $agent->getLastname(); ?></p>
                            <p>Codename: <?= $agent->getCodename(); ?></p>
                            <p>Birthdate: <?= date("d M Y", strtotime($agent->getBirthdate())); ?></p>
                            <p>Nationality: <?=$agent->getCountry(); ?></p>
                            <p>Skills: <?=implode( ", ", $agent->getSkills()); ?></p>
                        </div>
                    <?php
                    }
                    ?>

                    <?php
                    $targets = $mission->getTargets();
                    foreach ($targets as $target_aimed) {
                        $targetManager = new TargetManager();
                        $target = $targetManager->getAimedTarget($target_aimed);
                    ?>
                        <div class="missionPerson text-dark m-1 p-2">
                            <div class="text-center mb-2">
                            <svg class="m-3" width="50px" height="50px" viewBox="0 0 16 16" class="bi bi-bullseye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path fill-rule="evenodd" d="M8 13A5 5 0 1 0 8 3a5 5 0 0 0 0 10zm0 1A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"/>
                                <path fill-rule="evenodd" d="M8 11a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 1a4 4 0 1 0 0-8 4 4 0 0 0 0 8z"/>
                                <path d="M9.5 8a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
                            </svg>
                                <p class="personType text-uppercase font-weight-bold">Target</p>
                            </div>

                            <p><?= $target->getFirstname(); ?> <?= $target->getLastname(); ?></p>
                            <p>Codename: <?= $target->getCodename(); ?></p>
                            <p>Birthdate: <?= date("d M Y", strtotime($target->getBirthdate())); ?></p>
                            <p>Nationality: <?=$target->getCountry(); ?></p>
                        </div>
                    <?php
                    }
                    ?>
                    
                    <?php
                    $contacts = $mission->getContacts();
                    foreach ($contacts as $contact_solicited) {
                        $contactManager = new ContactManager();
                        $contact = $contactManager->getSolicitedContact($contact_solicited);
                    ?>
                        <div class="missionPerson text-dark m-1 p-2">
                            <div class="text-center mb-2">
                                <svg class="m-3" width="40px" height="50px" viewBox="0 0 16 16" class="bi bi-telephone-inbound-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511zM15.854.146a.5.5 0 0 1 0 .708L11.707 5H14.5a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5v-4a.5.5 0 0 1 1 0v2.793L15.146.146a.5.5 0 0 1 .708 0z"/>
                                </svg>
                                <p class="personType text-uppercase font-weight-bold">Contact</p>
                            </div>

                            <p><?= $contact->getFirstname(); ?> <?= $contact->getLastname(); ?></p>
                            <p>Codename: <?= $contact->getCodename(); ?></p>
                            <p>Birthdate: <?= date("d M Y", strtotime($contact->getBirthdate())); ?></p>
                            <p>Nationality: <?=$contact->getCountry(); ?></p>
                        </div>
                    <?php
                    }
                    ?>

                    <?php
                    $hideouts = $mission->getHideouts();
                    foreach ($hideouts as $hideout_needed) {
                        $hideoutManager = new HideoutManager();
                        $hideout = $hideoutManager->getNeededHideout($hideout_needed);
                    ?>
                        <div class="missionPerson text-dark m-1 p-2">
                            <div class="text-center mb-2">
                            <svg class="m-3" width="50px" height="50px" viewBox="0 0 16 16" class="bi bi-house-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 3.293l6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                                <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                            </svg>
                                <p class="personType text-uppercase font-weight-bold">Hideout</p>
                            </div>
                            <p>Codename: <?= $hideout->getCodename(); ?></p>
                            <p>Type: <?= $hideout->getType(); ?></p>
                            <p>Location: </p>
                            <p class="bg-light text-secondary p-2"><?=$hideout->getAddress(); ?></br>
                            <span class="text-uppercase"><?=$hideout->getCountry(); ?></span></p>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>

        </div>
        <?php
            }
        ?>

    </div>
    <!-- script -->
    <script src="./node_modules/jquery/dist/jquery.slim.min.js"></script>
    <script src="./node_modules/@popperjs/core/dist/umd/popper.js"></script>
    <script src="./node_modules/bootstrap/dist/js/bootstrap.js"></script>

</body>
</html>