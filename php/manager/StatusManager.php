<?php

require_once './php/manager/DBManager.php';
require_once './php/model/status.php';

class StatusManager extends DBManager{
    public function getAll($mission_code) {
        $result = [];

        $stmt_status = $this->getConnexion()->query('SELECT * FROM Actualize_status JOIN Status ON Status.status_num = Actualize_status.status_num WHERE mission_code ="' . $mission_code . '"');

        while($row_status = $stmt_status->fetch()) {
            $status = new Status();
            $status->setLabel($row_status['status_label']);
            $status->setDate($row_status['status_date']);

            $result[] = $status;
        }
        return $result;
    }

}