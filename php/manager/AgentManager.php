<?php

require_once './php/manager/DBManager.php';
require_once './php/model/agent.php';

class AgentManager extends DBManager{

    public function getRequiredAgent($agent_code) {
        $stmt_agent = $this->getConnexion()->query('SELECT * FROM Agent JOIN Country ON Agent.nationality = Country.country_num WHERE agent_code ="' . $agent_code . '"');

        $result = $stmt_agent->fetch();
        $agent = new Agent();
        $agent->setCodename($result['agent_code']);
        $agent->setLastname($result['agent_lastname']);
        $agent->setFirstname($result['agent_firstname']);
        $agent->setBirthdate($result['agent_birthdate']);

        $agent->setCountry($result['nationality']);
        
        $stmt_skill = $this->getConnexion()->query('SELECT skill_label FROM Specialize JOIN Skill ON Specialize.skill_num = Skill.skill_num  WHERE agent_code ="' . $result['agent_code'] . '"');
        $skills = $stmt_skill->fetchAll(PDO::FETCH_ASSOC);
        $agent->setSkills($skills);

        return $agent;
        
    }

}

