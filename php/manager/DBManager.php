<?php

/**
 * definition de la classe abstraite DBManager. Doit s'utiliser avec un "extends".
 * Paramètres de connexion à la base de données
 */
abstract class DBManager {
    private $connexion;

    public function __construct()
    {
        // $this->connexion = new PDO("mysql:host=localhost;dbname=secret_database;port=3306;charset=utf8mb4", "admin", "bunjevci");
        $this->connexion = new PDO("mysql:host=localhost;dbname=secret_database;port=3306;charset=utf8mb4", "Aecko", "qsdaze");
        
        // try {
        //     $connection = new PDO("mysql:localhost;port=3306;dbname=catalogue_edirol;charset=utf8mb4;" , "admin" , "bunjevci");
        //     echo "ça marche" . "<br>";
        // } catch(PDOException $e) {
        //     throw new PDOException($e->getMessage() , (int)$e->getCode());
        //     //die("ERREUR" . $e->getMessage());
        // }
    }

    public function getConnexion() {
        return $this->connexion;
    }
}
