<?php

class Typemission
{
    private int $num;
    private string $label;

    /**
     * Get the value of num
     */ 
    public function getNum(): int
    {
        return $this->num;
    }

    /**
     * Set the value of num
     *
     * @return  self
     */ 
    public function setNum(int $num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get the value of label
     */ 
    public function getLabel(): int
    {
        return $this->label;
    }

    /**
     * Set the value of label
     *
     * @return  self
     */ 
    public function setLabel(int $label)
    {
        $this->label = $label;

        return $this;
    }
}