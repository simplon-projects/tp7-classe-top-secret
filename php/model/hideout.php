<?php

class Hideout
{
    private string $codename;
    private string $address;
    private string $type;
    private string $country;

    /**
     * Get the value of codename
     */ 
    public function getCodename(): string
    {
        return $this->codename;
    }

    /**
     * Set the value of codename
     *
     * @return  self
     */ 
    public function setCodename(string $codename)
    {
        $this->codename = $codename;

        return $this;
    }

    /**
     * Get the value of address
     */ 
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @return  self
     */ 
    public function setAddress(string $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the value of type
     */ 
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */ 
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of country
     */ 
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * Set the value of country
     *
     * @return  self
     */ 
    public function setCountry(string $country)
    {
        $this->country = $country;

        return $this;
    }

    // public function __construct(string $codename, string $address, int $type, int $country)
    // {
    //     $this->codename = $codename;
    //     $this->address = $address;
    //     $this->type = $type;
    //     $this->country = $country;
    // }
}

