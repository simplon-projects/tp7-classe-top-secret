# TP7 - Classé Top Secret

## Objectif du projet

L’objectif est de concevoir un site internet permettant la gestion des données d'une agence secrète : 
1.  Création de la base de donnée (missions, agents, cibles, contacts, planques, ...);
2. Création d'une interface front-office (consultations des détails sur les missions, les agents, ...);
3. Création d'une interface back-office, accessible aux administrateurs uniquement via une page de login, depuis laquelle on peut modifier la BDD.

## Choix techniques et environnement de travail

Techno employées pour le projet :
- PHP 7,
- HTML5, 
- CSS3,
- JS ES6+,
- Bootstrap

## Contenu du dossier 

à compléter

## Installation

à compléter







